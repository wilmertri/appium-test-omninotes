/**
 * Created by wilme on 04/10/2018.
 */

const wdio = require("webdriverio");

const opts = {
    port: 4723,
    desiredCapabilities: {
        platformName: "Android",
        platformVersion: "8.0",
        deviceName: "Nexus 5 API 26",
        appPackage: "it.feio.android.omninotes",
        appActivity: "it.feio.android.omninotes.MainActivity",
        automationName: "appium"
    }
};

const client = wdio.remote(opts);

var button_next = 'new UiSelector().resourceId("it.feio.android.omninotes:id/next")';
var button_done = 'new UiSelector().resourceId("it.feio.android.omninotes:id/done")';
var button_ok = 'new UiSelector().resourceId("it.feio.android.omninotes:id/buttonDefaultPositive")';
var button_expand_menu = 'new UiSelector().className("android.widget.ImageButton").resourceId("it.feio.android.omninotes:id/fab_expand_menu_button")';
var button_add_note = 'new UiSelector().resourceId("it.feio.android.omninotes:id/fab_note")';
var title_note = 'new UiSelector().resourceId("it.feio.android.omninotes:id/detail_title")';
var content_note = 'new UiSelector().resourceId("it.feio.android.omninotes:id/detail_content")';
var input_search = 'new UiSelector().resourceId("it.feio.android.omninotes:id/search_src_text")';
var app_omni_notes = 'new UiSelector().text("Omni Notes")';
var nota_creada = 'new UiSelector().text("Prueba crear nota 1")';
var date = 'new UiSelector().text("20")';

function init() {
    client
        .init()
        .click("android=" + button_next)
        .click("android=" + button_next)
        .click("android=" + button_next)
        .click("android=" + button_next)
        .click("android=" + button_next)
        .click("android=" + button_done)
        .pause(1000)
        .click("android=" + button_ok)
        .pause(1000)
        .touchAction([
            { action: 'press', x: 200, y: 50 },
            { action: 'moveTo', x: 0, y: 200 },
            'release'
        ])
        .pause(2000)
        .click("android=" + button_expand_menu)
        .click("android=" + button_add_note)
        .setValue("android=" + title_note, "Prueba crear nota 1")
        .setValue("android=" + content_note, "Contenido para nota 1")
        .pause(2000)
        .click("~drawer open")
        .pause(2000)
        .click("android=" + button_expand_menu)
        .click("android=" + button_add_note)
        .setValue("android=" + title_note, "Prueba crear nota 2")
        .setValue("android=" + content_note, "Contenido para nota 2")
        .pause(2000)
        .click("~drawer open")
        .pause(2000)
        .click("~Search")
        .setValue("android=" + input_search, "nota 1")
        .pressKeycode('66')
        .pause(3000)
        .click("android=" + nota_creada)
        .pause(2000)
        .end();
}

function create_note() {
    client
        .click("~Apps list")
        .click("android=" + app_omni_notes)
        .pause(4000)
        .end();
}

init();
create_note();
